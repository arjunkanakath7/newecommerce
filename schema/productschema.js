const mongoose = require('mongoose');

const productschema = new mongoose.Schema({
    images: [String],
    mainimage: String,
    brand: String,
    title: String,
    price: Number,
    description: String
  });

  const Product = mongoose.model('Product', productschema);

  module.exports = Product