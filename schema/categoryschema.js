const mongoose = require('mongoose');

const categoryschema = new mongoose.Schema({
    categoryname: String,
    image: String
  });

  const Category = mongoose.model('Category', categoryschema);

  module.exports = Category